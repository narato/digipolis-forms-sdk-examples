# Examples of the ACPaaS Composer Core SDK

## Getting started

Clone the repository and install the dependencies
```
git clone https://stash.antwerpen.be/scm/acpfse/composer_examples_js.git
cd composer_examples_js
npm set registry https://npm.antwerpen.be
npm install
```

## Examples

To run an example from the examples folder, execute the `example` npm script and pass the path to the example as argument.
```
npm run example examples/hello-world.js
```

Some examples integrate with the ACPaaS Form & Survey Engine Template API. Update the configuration settings in `config.js` to run these examples successfully.