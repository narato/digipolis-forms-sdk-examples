/*
 * Save in Template API example
 *
 * Save a new template in the ACPaaS Form & Survey Engine Template API
 * Update the configuration in ../config.js to integrate successfully with the Template API
 */

import ComposerSdk from 'acpaas-composer-core-sdk';
import { FormInfo } from 'acpaas-composer-core-sdk';
import { Client } from 'node-rest-client';

import Config from '../config.js';

// initialize the REST Client
let client = new Client();
client.on('error', function (err) {
    console.log('Something went wrong. Did you update the configuration in config.js?', err);
});

// create a new form
let form = ComposerSdk.createForm();
let info = new FormInfo();
info.title = 'Yet another form';
form.info = info;

// save the form in the Template API
const args = {
    headers: {
        'content-type': 'application/json',
        accept: 'application/json',
        tenantKey: Config.templateApi.tenantKey
    },
    data: {
        name: 'my-new-template',
        description: 'A new test template',
        isDraft: false,
        content: form.toJSON()
    }
};
const baseUrl = `http://${Config.templateApi.host}:${Config.templateApi.port}/`;
client.post(baseUrl + 'api/Templates', args, function(data, response) {
    console.log('Response: ' + JSON.stringify(data, null, 2));
});
