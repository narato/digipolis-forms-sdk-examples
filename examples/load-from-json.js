/*
 * Load from JSON example
 *
 * Loading a template from a String into a Form object that can be used as a starting point
 */

import ComposerSdk from 'acpaas-composer-core-sdk';

// for brevity the JSON content is omitted here
// but this variable should hold your existing form template
let myJsonTemplate = `
{
    "name": "My example form",
    "formId": "uniqueExampleForm",
    "rendererVersion": "1",
    "info": {
        "title": "Example form title"
    }
}`;

let form = ComposerSdk.loadFromJSON(myJsonTemplate);

// now let's change the title to something else
form.info.title = 'This new title is much better!';

// no need to call any save method(s)
// just convert the form to JSON, your changes will be included
myJsonTemplate = JSON.stringify(form, null, 2);

// write the JSON to the console
console.log('Template JSON: ' + myJsonTemplate);
