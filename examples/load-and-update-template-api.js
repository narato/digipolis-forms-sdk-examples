/*
 * Load and update from Template API example
 *
 * Loading and updating an existing template in the ACPaaS Form & Survey Engine Template API
 * Update the configuration in ../config.js to integrate successfully with the Template API
 */

import ComposerSdk from 'acpaas-composer-core-sdk';
import { Client } from 'node-rest-client';

import Config from '../config.js';

// initialize the REST Client
let client = new Client();
client.on('error', function (err) {
    console.log('Something went wrong. Did you update the configuration in config.js?', err);
});

// load the latest version of the template with the specified key
const args = {
    headers: {
        accept: 'application/json',
        tenantKey: Config.templateApi.tenantKey
    },
    parameters: { scope: ['latest'] },
    path: { templateLookupKey: Config.templateApi.templateLookupKey }
};

// get the JSON template from the Template API
const baseUrl = `http://${Config.templateApi.host}:${Config.templateApi.port}/`;
client.get(baseUrl + 'api/Templates/${templateLookupKey}/versions', args, function(data, response) {
    let myJsonTemplate = data.data[0].content;
    let form = ComposerSdk.loadFromJSON(myJsonTemplate);

    // no need to call any save method(s)
    // just convert the form to JSON, your changes will be included
    myJsonTemplate = JSON.stringify(form, null, 2);

    // write the JSON to the console
    console.log('Loaded template JSON: ' + myJsonTemplate);

    // update the form
    form.addStep(ComposerSdk.createStep(`my-step-${new Date().getTime()}`));

    // Update the JSON template in the Template API
    const args = {
        headers: {
            'content-type': 'application/json',
            accept: 'application/json',
            tenantKey: Config.templateApi.tenantKey
        },
        data: {
            name: 'my-template',
            description: 'A test template',
            lookupKey: Config.templateApi.templateLookupKey,
            isDraft: true,
            content: form.toJSON()
        }
    };
    const baseUrl = `http://${Config.templateApi.host}:${Config.templateApi.port}/`;
    client.put(baseUrl + 'api/Templates', args, function(data, response) {
        console.log('Response: ' + JSON.stringify(data, null, 2));
    });
    
});
