/*
 * Hello world example
 *
 * Creating a simple form with a step, a section and a field
 */

import ComposerSdk from 'acpaas-composer-core-sdk';

let form = ComposerSdk.createForm();

// create a form section
let section = ComposerSdk.createSection('section0');
section.title = 'This is the section title';
section.subtitle = 'This is the subtitle of the section';

// create a form step
let step = ComposerSdk.createStep('step0');
step.title = 'This is the step title';
step.subtitle = 'This step also has a subtitle';
step.body = 'On top of that, we can also provide a body of content for our step';

// create a text input field
// prefilled with 'Hello World!'
let field = ComposerSdk.createField('field0');
field.spec.setTypeAttribute('input');
field.spec.setValueAttribute('Hello World!');

// add our field to the section
section.addField(field);
// add the section to our step
step.addSection(section);
// and finally add the step to our form
form.addStep(step);

// calling JSON.stringify on the form instance
// will output a JSON string that is compliant with the template spec
let json = JSON.stringify(form, null, 2);

// write the JSON to the console
console.log('Template JSON: ' + json);
