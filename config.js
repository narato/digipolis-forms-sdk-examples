export default {
    templateApi: {
        host: 'localhost',
        port: 5001,
        tenantKey: 'my-tenant-key',
        templateLookupKey: 'my-template-lookup-key'
    }
};
